sphinx-testing (1.0.1-0.1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:40:14 +0200

sphinx-testing (1.0.1-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
    - Compatible with Sphinx 2.x (closes: #955101, #955098).
  * Update upstream Homepage URL.
  * Stop using deprecated nose for tests, just unittest is enough.
  * Run the autopkgtest for all supported Python versions.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 14 Apr 2020 15:49:42 +0300

sphinx-testing (0.8.1-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Further drop remaining python2 packages; Closes: #938547

 -- Sandro Tosi <morph@debian.org>  Sat, 14 Mar 2020 23:55:05 -0400

sphinx-testing (0.8.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop python2 support; Closes: #938547

 -- Sandro Tosi <morph@debian.org>  Sun, 13 Oct 2019 00:07:12 -0400

sphinx-testing (0.8.1-1) unstable; urgency=medium

  * New upstream release
    - Fixe issue (Closes: #914474).
  * debian/control
    - Standards-Version: Changes 3.9.8 to 4.3.0.
    - Removes Python-Version: 2.7
    - Removes X-Python3-Version: >= 3.2
  * debian/rules
    - Removes PYBUILD_BEFORE_TEST, PYBUILD_TEST_ARGS.
    - Removes override_dh_python3, override_dh_python2.
  * debian/watch
    - Fixes debian-watch-uses-insecure-uri.
  * debian/patches
    - Fixes remove-non-local-image-link.
    - Deletes remove-failing-tests.patch.
  * Adds debian/tests
    - Adds control, python-sphinx-testing, python3-sphinx-testing.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Sat, 05 Jan 2019 09:04:36 +0900

sphinx-testing (0.7.2-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release:
    - Fix issue with Sphinx 1.6 (Closes: #875399).
  * Added debian/source/options to allow ignoring generated egg-info folder,
    which makes it possible to build the package twice in a raw.

 -- Thomas Goirand <zigo@debian.org>  Wed, 11 Oct 2017 09:46:31 +0000

sphinx-testing (0.7.1-2) unstable; urgency=medium

  * debian/control
    - Standards-Version: Bumps version to 3.9.8.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Tue, 11 Oct 2016 15:27:47 +0900

sphinx-testing (0.7.1-1) unstable; urgency=medium

  * New upstream release
  * Uploading to unstable
  * debian/watch
    - Fixes debian-watch-file-unsupported-pypi-url.
  * debian/patches/remove-nonlocal-image-link.patch
    - Appends removing external links.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Wed, 01 Jul 2015 14:37:26 +0900

sphinx-testing (0.7.0-1) experimental; urgency=medium

  * New upstream release
  * debian/watch
    - Updated url.

 -- Kouhei Maeda <mkouhei@palmtb.net>  Sun, 19 Apr 2015 07:41:00 +0900

sphinx-testing (0.6.0-2) unstable; urgency=medium

  * Fixed dist-packages/tests/__init__.py conflict (Closes: #767397)
    - debian/rules
      * Appdended override_dh_python3, override_dh_python2.
    - Thanks for report "Aaron M. Ucko" <ucko@debian.org>

 -- Kouhei Maeda <mkouhei@palmtb.net>  Fri, 31 Oct 2014 11:23:50 +0900

sphinx-testing (0.6.0-1) unstable; urgency=low

  * Initial release (Closes: #766494)

 -- Kouhei Maeda <mkouhei@palmtb.net>  Fri, 24 Oct 2014 00:05:01 +0900
